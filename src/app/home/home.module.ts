import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }    from '@angular/forms';

import { HomeRoutingModule } from './home-routing.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ModalModule } from 'dsg-ng2-bs4-modal/ng2-bs4-modal';
import { HomeComponent } from './home/home.component';
import { PopupModule } from 'ng2-opd-popup';

import { DeudaMoraComponent } from './deudamora/deudamora.component';
import { InmueblesComponent } from './inmuebles/inmuebles.component';
import { ActividadesComponent } from './actividades/actividades.component';
import { VehiculosComponent } from './vehiculos/vehiculos.component';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    Ng2SmartTableModule,
    
    HomeRoutingModule,
    PopupModule.forRoot()
  ],
  declarations: [
  		HomeComponent,
  		DeudaMoraComponent,
      InmueblesComponent,
      ActividadesComponent,
      VehiculosComponent
]
})
export class HomeModule { }
