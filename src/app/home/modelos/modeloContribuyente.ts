export class ModeloContribuyente {
    IDPERSONA: number;
    APELLIDO: string;
    APELLIDO2: string;
    NOMBRE: string;
    PADRON: string;
    identificacion: string;
    ESTADO: string;
}